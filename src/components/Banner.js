import { Button, Row, Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({data}) {

	const { title, content, destination, label } = data;

	return (

		<>
		<Row className="mb-3">
			<Col className="p-5 text-center" xs={12} md={12}>
				<h1>{title}</h1>
				{/*<img alt = "the car provider you'll have" aligm= "center" height="10%" width= "auto" src="https://img.freepik.com/premium-vector/car-logo-design-concept-illustration-icon-brand-identity_7109-1053.jpg"></img>*/}
				<p>{content}</p>
				<Button as={Link} to={destination} variant="primary">{label}</Button>
			</Col>

		</Row>

		<Row className="mt-3 mb-3" >
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-1" bg = "primary">
				     <Card.Body>
				       <Card.Title>
				       		<img alt = "the car provider you'll have" aligm= "center" height="100px" width= "100px" src="https://static.thenounproject.com/png/4772174-200.png"></img>
				       		<h2>Easy Payment Terms</h2>
				       </Card.Title>
				       <Card.Text>
				       		You have the option of low Dp or low monthly amortization.
				       </Card.Text>
				     </Card.Body>
				   </Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-1" bg = "danger">
				     <Card.Body>
				       <Card.Title>
				       		<img alt = "the car provider you'll have" aligm= "center" height="100px" width= "100px" src="https://cdn-icons-png.flaticon.com/512/79/79869.png"></img>
				       		<h2>Test Drive, To feel it</h2>
				       </Card.Title>
				       <Card.Text>
				         Test drive the car you want, we are glad to have it tested the way it will amaze you.
				       </Card.Text>
				     </Card.Body>
				   </Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-1" bg = "success">
				     <Card.Body>
				       <Card.Title>
				       		<img alt = "the car provider you'll have" aligm= "center" height="100px" width= "100px" src="https://cdn.iconscout.com/icon/premium/png-256-thumb/support-2807743-2336539.png"></img>
				       		<h2>Best after sale Service</h2>
				       </Card.Title>
				       <Card.Text>
				       	We will take good care of your car at very affordable price. We have stocks of genuine parts if you need replacements.
				       </Card.Text>
				     </Card.Body>
				   </Card>



			</Col>
			</Row>
			</>
		
	)
}