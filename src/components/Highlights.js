import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights(){
	return (
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-1">
				     <Card.Body>
				       <Card.Title>
				       		<h2>Easy Payment Terms</h2>
				       </Card.Title>
				       <Card.Text>
				       		You have the option of low Dp or low monthly amortization.
				       </Card.Text>
				     </Card.Body>
				   </Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-1">
				     <Card.Body>
				       <Card.Title>
				       		<h2>Test Drive, To feel it</h2>
				       </Card.Title>
				       <Card.Text>
				         Test drive the car you want, we are glad to have it tested the way it will amaze you.
				       </Card.Text>
				     </Card.Body>
				   </Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-1">
				     <Card.Body>
				       <Card.Title>
				       		<h2>Good after sale Service</h2>
				       </Card.Title>
				       <Card.Text>
				       	We will take good care of your car at very affordable price. We have stocks of genuine parts if you need replacements.
				       </Card.Text>
				     </Card.Body>
				   </Card>
			</Col>
		</Row>
	)
}