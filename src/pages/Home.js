

import Banner from '../components/Banner';
// import Highlights from '../components/Highlights';



export default function Home() {

	const data = {
		title : "Car4U",
		content : "Need A Ride, You have it Here!",
		destination: "/products",
		label: "Check available Cars!"
	}
	return (
		<>

				<Banner data={data}/>
				{/*<Highlights/>*/}
			
		</>
	)
}