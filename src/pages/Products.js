import { useEffect, useState } from 'react';
import ProductCard from '../components/ProductCard';

export default function Products() {

	// State that will be used to store the courses retrieved from the database
	const [ products, setProducts ] = useState([]);

	// Retrieves the products from the database upon intial render of the "products" component

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			
			setProducts(data.map(product => {
				return (
					<ProductCard key={product._id} productProp = {product}/>
				)
			}))
		})
	}, [])




	return (
		<>
			<h3>Product Catalog</h3>
			{products}
			
		</>

	)
}