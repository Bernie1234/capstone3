import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout(){

	// Consume the UserContext object and desctructure it to access the user state and unsetUser function from the context provider
	const { unsetUser, setUser } = useContext(UserContext);

	/*
	"localStorage.clear()" is a method that allows us to clear the information in the localStorage ensuring that no information is stored in our browser
	*/
	// localStorage.clear()

	// Clears the localStorage of the user's information
	unsetUser();

	useEffect(() => {
		setUser({id: null});
	})

	// Redirects back to login
	return (
		<Navigate to='/login' />
	)
}